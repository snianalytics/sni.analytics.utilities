module.exports.ProcessThenMove = function (source, target, debug, processCall) {

    var fs = require('fs');
    var fileExtension = require('file-extension');

    source_unprocessed = process.cwd() + "/" + source + "/unprocessed/";
    source_processed = process.cwd() + "/" + source + "/processed/";
    target_unprocessed = process.cwd() + "/" + target + "/unprocessed/";

    fs.readdir(source_unprocessed, function (err, items) {

        console.log("Path==>" + source_unprocessed + " items- " + items.length);

        for (var k = 0; k < items.length; k++) {

            if (fileExtension(items[k]) == 'json') {

                var object = require(source_unprocessed + items[k]);

                var filename = items[k];
                var charlesFilename = filename.slice(0, -5) + ".chls";

                console.log('Parsing logs for ... ' + filename);
                console.log('---');
                try {
                    success = processCall(object, filename, target_unprocessed);

                    if (!debug && !success) {
                        console.log('Moving file --> ' + filename);
                        module.exports.MoveFile(source_unprocessed, source_processed, filename);

                        console.log('Moving file --> ' + charlesFilename);
                        module.exports.MoveFile(source_unprocessed, source_processed, charlesFilename);
                    }
                } catch(err) {
                    return false;
                }
                console.log("******************************");

            }
        }
    });
}
module.exports.parseKeyValue = function(name, value, callObject) {

    if (value != undefined) {
        if (decodeURIComponent(String(value)).length >= 13) {
            callObject.fieldSet[decodeURIComponent(name)] = '="' + decodeURIComponent(value) + '"';
        } else {
            callObject.fieldSet[decodeURIComponent(name)] = decodeURIComponent(value);
        }
    }
   /*  if (value != "") {
        if (Type == type.Analytics) {
            analyticsheaderObject[name] = '';

        } else if (Type == type.Heartbeat) {
            hb_headerObject[name] = '';

        }
        // headerObject[name] = '';
    } */


}
module.exports.MoveFile = function (source, target, filename) {
    var fs = require('fs');

    try {
        fs.statSync(target);
    } catch (e) {
        fs.mkdirSync(target);
    }

    if (fs.existsSync(source + filename)) {
        fs.rename(source + filename, target + filename, function (err) {
            if (err) {
                console.log("Error: " + err);
                throw err
            }
        })
    };

}